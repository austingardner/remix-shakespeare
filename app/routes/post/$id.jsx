import { useLoaderData, Link } from 'remix';

export const loader = async ({ params }) => {
  console.log(params);
  const data = await fetch(
    `https://shakespeare.podium.com/api/reviews/${params.id}`,
    {
      headers: {
        'x-api-key': 'H3TM28wjL8R4#HTnqk?c',
      },
    }
  ).then((d) => d.json());
  return data;
};

export default function PostId() {
  const { author, body, publish_date: date } = useLoaderData();

  return (
    <div className="flex flex-col h-screen justify-center items-center flex-wrap">
      <span className="p-8 w-200 m-5 rounded overflow-hidden shadow-lg">
        <span className="px-6 py-4">
          <div className="font-bold text-2xl mb-2 text-center">{author}</div>
          <div className="font-bold text-2xl mb-2 text-center">
            {new Date(date).toLocaleDateString()}
          </div>
          <p className="text-gray-700 text-xl">{body}</p>
          <Link style={{ color: 'blue' }} className="mt-10" to={`/posts/`}>
            Back to posts list
          </Link>
        </span>
      </span>
    </div>
  );
}
