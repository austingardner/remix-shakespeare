import { Link } from 'remix';

export default function Index() {
  return (
    <div className="flex flex-col justify-center items-center h-screen">
      <h1 className="text-2xl font-bold">Welcome to Shakespeare's Reviews</h1>
      <Link style={{ color: 'blue' }} to="/posts">
        Enter
      </Link>
    </div>
  );
}
