import { useLoaderData, Link } from 'remix';

export const loader = async () => {
  const data = await fetch('https://shakespeare.podium.com/api/reviews', {
    headers: {
      'x-api-key': 'H3TM28wjL8R4#HTnqk?c',
    },
  }).then((d) => d.json());
  return data;
};

export default function Post() {
  const posts = useLoaderData();

  return (
    <div className="flex flex-col justify-center flex-wrap mt-3">
      <Link to={`/`}>
        <span className="border-solid border-2 border-indigo-600 p-3 m-2 rounded-md">
          Home 🏡
        </span>
      </Link>
      <div className="flex flex-wrap justify-center m-auto">
        {posts.map(({ author, body, id }) => (
          <span
            key={id}
            className="p-8 h-32 w-80 m-5 rounded overflow-hidden shadow-lg transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300"
          >
            <span className="px-6 py-4">
              <span className="font-bold text-xl mb-2">{author}</span>
              <p className="text-gray-700 text-base truncate">{body}</p>
              <Link style={{ color: 'blue' }} to={`/post/${id}`}>
                See More
              </Link>
            </span>
          </span>
        ))}
      </div>
    </div>
  );
}
