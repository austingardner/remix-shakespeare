module.exports = {
  content: ['./app/**/*.{ts,tsx,jsx,js}'],
  theme: {
    fontFamily: {
      sans: ['system-ui', 'sans-serif'],
    },
    extend: {},
  },
  variants: {},
  plugins: [],
};
